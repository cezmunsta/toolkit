#!/usr/bin/env python

import sys
import os
import time
import struct
import hmac
import hashlib
import base64
import logging
import subprocess
import shlex
import re

DEBUG = False

attempts = 0
attempts_max = 3

# Logging
logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s %(levelname)s %(message)s',
    filename='/var/log/ssh-auth.log',
    filemode='a'
)

# Authentication Error
class SSHAuthError(Exception):
	pass

# Forced Command
class ForcedCommand(Exception):
	pass

# Authenticator
def authenticate(secretkey, code_attempt):
    tm = int(time.time() / 30)
    secretkey = base64.b32decode(secretkey)

    for ix in [-1, 0, 1]:
        b = struct.pack(">q", tm + ix)
        hm = hmac.HMAC(secretkey, b, hashlib.sha1).digest()
        offset = ord(hm[-1]) & 0x0F
        truncatedHash = hm[offset:offset+4]

        code = struct.unpack(">L", truncatedHash)[0]
        code &= 0x7FFFFFFF;
        code %= 1000000;

        if ("%06d" % code) == str(code_attempt):
            return True
    return False

if len(sys.argv) < 2:
        sys.exit("USAGE: {0} code".format(__file__))

secret = {
            'default': "KJOJCWCXUCC2AAQO"
        }.get(sys.argv[1], "KJOJCWCXUCC2AAQO")

try:
	action = os.getenv('SSH_ORIGINAL_COMMAND')
	if action is None:
		action = os.getenv('SHELL')
	if re.match('^(/usr/bin/)?rsync', action):
		logging.info('SSH_ORIGINAL_COMMAND = {0}'.format(action))
		raise ForcedCommand()
	code_attempt = raw_input('Enter your key: ')
	if not authenticate(secret, code_attempt.strip()):
		raise SSHAuthError() 
except ForcedCommand:
	pass
except KeyboardInterrupt:
	sys.exit(0)
except SSHAuthError:
	message =  'Access denied'
	logging.error('{msg} - {cmd} - {user}'.format(msg=message, cmd=action, user=os.getenv('USER')))
	sys.exit(message)
except:
	sys.exit('Unauthorised access')

"""
if sys.argv[1] == 'restricted':
	command_permitted = {
			'/bin/sh': True,
			'/bin/bash': True,
			'/usr/bin/scp': True,
			'/usr/bin/rsync': True
			}.get(action, False)
	if not command_permitted:
		sys.exit("Access denied")
"""
subprocess.call(shlex.split(action))
