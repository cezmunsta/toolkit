#!/usr/bin/python

import argparse
import csv
import re


class PasswordChecker(object):
    UPPER_CASE = [chr(d) for d in range(65, 91)]
    LOWER_CASE = [chr(d) for d in range(97, 123)]
    DIGITS = [str(d) for d in range(0, 10)]
    SPECIAL = [chr(d) for d in range(34, 48)] + \
              [chr(d) for d in range(59, 65)] + \
              [chr(d) for d in range(123, 127)]
    
    FLAG_DIGITS = 1
    FLAG_UPPER = 2
    FLAG_LOWER = 4
    FLAG_SPECIAL = 8


    def __init__(self, csv_file, min_length=20, digits=1, upper_case=1, lower_case=1, special=1):
        self.patterns = {
            'DIGITS': re.compile('(.*[{0}].*){{{1:d},}}'.format(''.join(self.DIGITS), digits)),
            'UPPER':  re.compile('(.*[{0}].*){{{1:d},}}'.format(''.join(self.UPPER_CASE), upper_case)), 
            'LOWER': re.compile('(.*[{0}].*){{{1:d},}}'.format(''.join(self.LOWER_CASE), lower_case)),
            'SPECIAL':  re.compile('(.*[{0}].*){{{1:d},}}'.format(''.join(self.SPECIAL), special)),
        }
        self.min_length = min_length
        self.csv_file = csv_file
        self.filters = '.*'

        self.__results = []


    @property
    def __passwords(self):
        data = []
        with open(self.csv_file) as csvfile:
            reader = csv.DictReader(csvfile)
            try:
                for row in reader:
                    data.append({'{0} - {1}'.format(row['grouping'], row['name']): row['password']})
            except KeyError:
                pass  # TODO: add warning about bad format
        return data

    @property
    def results(self):
        if not len(self.__results):
            self.__check_passwords()
        return self.__results

    
    def __check_complexity(self, pw):
        _status = 0
        
        if not len(pw) >= self.min_length:
            return False

        for pattern in list(self.patterns.keys()):
            if self.patterns[pattern].match(pw):
                _status += getattr(self, 'FLAG_{0}'.format(pattern))
    
        return _status & (self.FLAG_DIGITS + self.FLAG_UPPER + self.FLAG_LOWER + self.FLAG_SPECIAL) == \
            (self.FLAG_DIGITS + self.FLAG_UPPER + self.FLAG_LOWER + self.FLAG_SPECIAL)


    def __check_passwords(self):
        filtered_account = re.compile(self.filters) if self.filters != '.*' else False

        for pw in self.__passwords:
            pw_item = list(pw.values()).pop()
            pw_len = len(pw_item)
            pw_key = list(pw.keys()).pop()

            if pw_len == 0:
                continue
            elif filtered_account is not False and not filtered_account.match(pw_key):
                continue

            self.__results.append(('PASS' if self.__check_complexity(pw_item) else 'FAIL', 
                                    pw_len, pw_key,))
        
        return len(self.__results) > 0


def main():
    parser = argparse.ArgumentParser(prog='lpcheck', description='LastPass checker')
    main_group = parser.add_argument_group('main options')
    main_group.add_argument('CSV_FILE', default='lastpass.csv', metavar='CSV_FILE',
                            nargs='?', help='Set the CSV source file')
    main_group.add_argument('--min', '-m', default=20, type=int, 
                            help='Minimum length threshold (default: %(default)s)')
    main_group.add_argument('--upper', '-u', default=1, type=int, 
                            help='Count of uppercase characters (default: %(default)s)')
    main_group.add_argument('--lower', '-l', default=1, type=int, 
                            help='Count of lowercase characters (default: %(default)s)')
    main_group.add_argument('--digits', '-n', default=1, type=int,
                            help='Count of numeric characters (default: %(default)s)')
    main_group.add_argument('--special', '-s', default=1, type=int,
                            help='Count of special characters (default: %(default)s)')
    main_group.add_argument('--filter', '-f', default='.*', help='Filter accounts')
    main_group.add_argument('--debug', '-d', action='store_true', help='Debug mode')
    args = parser.parse_args()

    password_check = PasswordChecker(args.CSV_FILE, args.min, args.digits, args.upper,
                                     args.lower, args.special)
    password_check.filters = args.filter

    for result in password_check.results:
        print ("{0}\t{1:d}\t{2}".format(*result))


if __name__ == '__main__':
    main()
