#!/bin/bash

set -eu

declare -i RETVAL=0
declare -i DEBUG=0
declare -i VERBOSE=0
declare -i DRYRUN=1

declare -r PARAMS=":hdveH:"
declare -r PROGNAME=$(basename ${0})
declare -r FW_DIR='/etc/fw'
declare -r CHANGELOG='changelog'
declare -r HOSTS_DB='hosts.db'
declare -r DBQ='/usr/bin/sqlite3'

declare HOSTN=''
declare IPADDR=''
declare QUERY='select hostname, ip_address from dns'

usage() {
	echo -e "Usage: ${PROGNAME}\n"
}

message() {
	echo -e "$(date --rfc-3339=seconds) ${*}" 
}

debug() {
	test ${DEBUG} -ne 1 && return
	message "DEBUG: ${*}"
}

info() {
	test ${VERBOSE} -ne 1 && return
	message "INFO: ${*}"
}

error() {
	message "ERROR: ${*}"
}

while getopts ${PARAMS} opt; do
	case ${opt} in
		h) usage; exit 0;;
		d) DEBUG=1;;
		v) VERBOSE=1;;
		e) DRYRUN=0;;
		H) QUERY="${QUERY} where hostname='${OPTARG}'";;
		\:) error "${OPTARG} requires a value"; exit 1;
	esac
done

info "Starting host check"
debug "Checking environment"

test -d ${FW_DIR} || exit 1
cd ${FW_DIR}

debug "Zeroing changelog"
echo -n '' > ${CHANGELOG} 

for row in $(${DBQ} ${HOSTS_DB} "${QUERY}"); do
	HOSTN=$(echo ${row} | cut -f1 -d'|')
	IPADDR=$(echo ${row} | cut -f2 -d'|')

	debug "Checking host '${HOSTN}' has address '${IPADDR}'"
	tmp=$(/usr/bin/host ${HOSTN} | /bin/grep -oP '([0-9]{1,3}.){3}[0-9]{1,3}')
	test "${tmp}" != "${IPADDR}" && {
		info "Updating IP address for '${HOSTN}' to '${tmp}'"
		${DBQ} ${HOSTS_DB} "update dns set ip_address = '${tmp}', modified = DATETIME('now') where hostname = '${HOSTN}'"
		echo ${tmp} >> ${CHANGELOG}
	} 
done

test -s ${CHANGELOG} && {
	debug "Applying firewall"
	test ${DRYRUN} -eq 0 && /etc/fw/cloud.fw
}

echo -n > ${CHANGELOG}

info "Completed"

exit ${RETVAL}
