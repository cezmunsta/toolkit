#!/bin/bash

declare -r PROGNAME=$(basename ${0})
declare -r PARAMS=':hvdu:r:'
declare -r DTS=$(date +%Y%m%d)
declare -r LOCKFILE="/var/run/$(echo ${PROGNAME} | cut -f1 -d.).pid"
declare -r DATA_DIR='/usr/local/share/geoip'
declare -r LOG_DIR='/var/log'

declare -i RETVAL=0
declare -i VERBOSE=0
declare -i DEBUG=0

declare DATA_URI='http://geolite.maxmind.com/download/geoip/database/GeoLiteCountry/GeoIP.dat.gz'
declare RECIPIENTS='admin@incoming-email.co.uk'

source /usr/local/include/utils/functions.sh

set -u
trap 'terminate;' ERR
trap 'echo Aborted; remove_lock; exit 255;' SIGABRT SIGTERM SIGHUP SIGQUIT

function usage {
	local padding='    '
	echo "
Usage: ${PROGNAME} [options]

${padding}-u uri    Download location for the GeoIP Country data
${padding}-r list   List of email recipients
${padding}-v        Verbose mode
${padding}-d        Debug mode
${padding}-h        Show this information
        "
}

while getopts "${PARAMS}" opt; do
	case ${opt} in
		h)   usage; exit 0;;
		u)   DATA_URI="${OPTARG}";;
		r)   RECIPIENTS="${OPTARG}";;
		:)   error "'${OPTARG}' requires a value"; exit 2 ;;
		\?)  error "Unknown option -${OPTARG}"; exit 1 ;;
	esac
done

message "Starting GeoIP update"
add_lock 

test ! -d ${DATA_DIR} && {
	mkdir -p ${DATA_DIR}
}

wget -o ${LOG_DIR}/geoip-updates.log -O ${DATA_DIR}/latest.dat.gz "${DATA_URI}"
cd ${DATA_DIR} && gunzip -f latest.dat.gz
cd /

remove_lock 
message "Done" 

exit ${RETVAL}
