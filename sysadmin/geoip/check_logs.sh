#!/bin/bash

source /etc/profile

declare -r PROGNAME=$(basename ${0})
declare -r PARAMS=':hysc:w:'

declare -i AUTOMODE=0
declare -i SAVEFW=0

declare -a CN_LIST=( GB US )
declare -a WHITELIST=( 89.234.35.100 )

declare BLACKLIST='/etc/fw/blacklist.txt'
declare BADBOTS='/etc/fw/badbots.txt'
declare CMDSAVE='/etc/fw/commands.save'

function usage {
	local padding='    '
	echo "
Usage: ${PROGNAME} [options]

${padding}-c a,b,c  Set the accepted country list
${padding}-w a,b,c  Set the IP whitelist
${padding}-y        Run in auto mode
${padding}-s        Save the firewall config
${padding}-h        Show this information
        "
}

while getopts "${PARAMS}" opt; do
	case ${opt} in
		h)   usage; exit 0;;
		y)   AUTOMODE=1 ;;
		s)   SAVEFW=1 ;;
		c)   CN_LIST=( $(echo ${OPTARG} | tr , ' ') );;
		w)   WHITELIST=( $(echo ${OPTARG} | tr , ' ') );;
		:)   echo "ERROR: '${OPTARG}' requires a value"; exit 2 ;;
		\?)  echo "ERROR: Unknown argument -${OPTARG}"; exit 1 ;;
	esac
done

cp ${BLACKLIST} ${BLACKLIST}.tmp
cp ${BADBOTS} ${BADBOTS}.tmp

# https://support.google.com/webmasters/answer/80553?hl=en
# http://www.bing.com/webmaster/help/how-to-verify-bingbot-3905dc26

for bot in Googlebot Bingbot; do
        case ${bot} in
                Googlebot) prefix='crawl-'; suffix='googlebot.com';;
                Bingbot) prefix='msnbot-'; suffix='search.msn.com';;
        esac
        for ip in $(grep -iP ${bot} /var/log/nginx/ftga_access.log | cut -f1 -d' ' | sort -u); do 
                crawldomain="${prefix}-$(echo ${ip} | tr . -).${suffix}."
                message=PASS
                rdns=$(host ${ip} | tail -n1)
                test $(echo ${rdns} | grep -cP "${crawldomain}" ) -eq 0 && message=FAIL
                test $(echo $(host ${crawldomain}) | grep -cP "${crawldomain}" ) -eq 0 && message=FAIL
                test message == FAIL && echo ${ip} >> ${BADBOTS}.tmp
        done
done

for ip in $(cat /var/log/nginx/ftga_access.log | cut -f1 -d' ' | sort -u); do 
        cn=$(geoiplookup -f /usr/local/share/geoip/latest.dat ${ip} | sed 's/GeoIP Country Edition: //' | cut -f1 -d,); 
        test $(echo "${CN_LIST[@]}" | grep -c $cn) -eq 0 && echo ${ip} >> ${BLACKLIST}.tmp;
done

cat ${BLACKLIST}.tmp | sort -u > ${BLACKLIST}
cat ${BADBOTS}.tmp | sort -u > ${BADBOTS}
rm -f ${BLACKLIST}.tmp ${BADBOTS}.tmp

echo "$(cat ${BLACKLIST} | wc -l) IPs from blocked countries"
echo "$(cat ${BADBOTS} | wc -l) bot IPs pretending to be Google/MSN"

echo "/sbin/iptables -F;" > ${CMDSAVE}
echo "" > ${CMDSAVE}.tmp

for ip in ${WHITELIST}; do
	echo "/sbin/iptables -A INPUT -s ${ip} -j ACCEPT;" >> ${CMDSAVE}
done

while read ip; do
	echo "/sbin/iptables -A INPUT -s ${ip} -j DROP;"  >> ${CMDSAVE}.tmp
done < ${BLACKLIST}

while read ip; do
	echo "/sbin/iptables -A INPUT -s ${ip} -j DROP;" >> ${CMDSAVE}.tmp
done < ${BADBOTS}

cat ${CMDSAVE}.tmp | sort -u >> ${CMDSAVE}
rm -fv ${CMDSAVE}.tmp

test ${AUTOMODE} -eq 1 && {
	ANSWER=y
} || {
	echo "Preview of changes (first 100 lines):"
	echo ""
	head -n100 ${CMDSAVE}
	echo ""
	echo "Do you wish to apply the firewall changes? y/n"
	read ANSWER
}

test $(echo ${ANSWER} | tr [:upper:] [:lower:]) = y && {
	source ${CMDSAVE}
	test ${SAVEFW} -eq 1 && /sbin/service iptables save
}

exit 0
