#!/bin/bash

declare -r HOME_IP='192.168.40'
declare -i DEBUG=${1:-0}

function check_ip {
    check_route || { echo noroute; return; } 
    using_vpn && { echo vpn; return; }
    at_work && { echo work; return; }
    at_home && { echo home; return; }
    echo roaming
}

function check_route {
    local default_route=$(route -n | fgrep 0.0.0.0)
    local -i status=0
    
    test -z "${default_route}" && status=1

    return ${status}
}

function at_work {
    local -i status=0
    local at_work=$(host -W 1 not.iovox 192.168.0.1 >/dev/null 2>&1; echo $?)

    test ${at_work} -eq 0 || status=1

    return ${status}
}

function using_vpn {
    local -i status=0
    local using_vpn=$(route -n | fgrep -q ppp0; echo $?)

    test ${using_vpn} -eq 0 || status=1

    return ${status}
}

function at_home {
    local ip_list=$(ip addr | fgrep 'inet ' | sed -r 's/(.*inet )([0-9.]+)(.*)/\2/g')
    local -i home_ip=$(echo -e "${ip_list}" | fgrep -c ${HOME_IP})
    local -i status=0

    test ${home_ip} -ne 0 || status=1
 
    return ${status}
}

function update_ssh_config {
    local ssh_config=${1:-na}

    test -L ~/.ssh/config && \
    test -f ~/.ssh/config_${ssh_config} && \
    test $(ls -l ~/.ssh/config | fgrep -c config_${ssh_config}) -eq 0 && {
        echo "Updating SSH config to ${ssh_config}"
        ln -sf ~/.ssh/config_${ssh_config} ~/.ssh/config
    }
}

case $(check_ip) in
    noroute) ;;
    vpn)    update_ssh_config vpn;;
    work)   update_ssh_config work;;
    home|*) update_ssh_config roaming;;
esac

