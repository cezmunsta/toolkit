#!/bin/bash
# vim: set ts=4 sw=4 expandtab:

set -e

declare -r PROGNAME=$(basename ${0})
declare -r LIB_DIR=$(dirname ${BASH_SOURCE})
declare -r PROGDIR=$(dirname ${PROGNAME})
declare -i RETVAL=0
declare -i DEBUG=0
declare -i VERBOSE=0

declare -A ERR_CODES=( 
    [misc]=1 
    [file]=2 
    [unknown]=255
)
declare -A ERR_MSGS=(
    [misc]="something went wrong"
    [file]="file not found"
    [unknown]="an unknown error occurred"
)

declare USAGE_INFO="TBC"

source ${LIB_DIR}/utils/functions.sh
