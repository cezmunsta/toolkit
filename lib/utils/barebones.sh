#!/bin/bash

set -u

declare -r PROGDIR=$(dirname ${0})
declare -r PROGNAME=$(basename ${0})

declare -i RETVAL=0
declare -i DEBUG=0
declare -i VERBOSE=0
declare -i PSTART=0
declare -i PSEND=0
declare -i PTIME=0

declare PARAMS=':hdv'
declare USAGE="
Usage: ${PROGNAME}

    -d           Enable debug mode
    -v           Enable verbose output
    -h           Show this information
    "

usage() {
    echo -e "${USAGE}"
}

get_tstamp() {
    #date --rfc-3339=seconds
    date +"%Y-%m-%d %T %Z"
}

message() {
    echo -e "$(get_tstamp) ${@}" 1>&2
}

debug() {
    test ${DEBUG} -ne 0 || return
    message "DEBUG: ${*}"
}

info() {
    test ${VERBOSE} -ne 0 || return
    message "INFO: ${*}"
}

warn() {
    message "WARNING: ${*}"
}

error() {
    message "ERROR: ${*}"
}

backtrace() {
        local frame=0

        while caller ${frame}; do
                ((frame++))
        done

        echo "${*}"
        exit 1
}

cleanup() {
	debug 'Cleanup task'
}

run() {
	warn 'This script does nothing, you need to define your run method'
}

main() {
	trap 'backtrace' SIGHUP SIGINT SIGQUIT SIGILL SIGABRT SIGKILL SIGSEGV SIGTERM
	trap 'cleanup' EXIT

	info "Starting ${PROGNAME}"

	PSTART=$(date +%s)
	run
	PSEND=$(date +%s)
	PTIME=$(echo "$PSEND - $PSTART" | bc)

	debug "Process took ${PTIME} second$(test ${PTIME} -ne 1 && echo s) to run"
	info "Done"

	exit ${RETVAL}
}
