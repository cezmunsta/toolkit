#!/bin/bash
# vim: set ts=4 sw=4 expandtab:

function usage {
    local status=${1}

    printf "
    #Usage: ${PROGNAME} [-h] ${USAGE_INFO}
    #   -h  Show this information
    #   -d  Enable debugging
    #   -v  Enable verbose logging

    " | sed -r 's/^[[:space:]]+//g; s/#//g'

    test -n "${status}" && exit ${status}
}


######################################################################
# Logging
#####

function message {
    local ts=$(date --rfc-3339=seconds)
    printf "${ts} ${@}\n"
}

function info {
    [[ ${VERBOSE} -gt 0 ]] && message "INFO: ${@}"
}

function debug {
    [[ ${DEBUG} -gt 0 ]] && message "DEBUG: ${@}"
}

function warn {
    message "WARN: ${@}"
}

function error {
    message "ERROR: ${@}"
}

function crit {
    message "CRIT: ${@}"
}

function log {
    local -a data=( $(echo "${@}") )
    local -i pos=$((${#data[@]} - 1))
    local level=${1:-message}
    local msg="${data[@]:1:${pos}}"

    case ${level} in
        message|verbose|debug|warn|error|crit) $level "${msg}" 1>&2;;
        *) message "${msg}" 1>&2;; 
    esac
}

function funcmark {
    log debug "${FUNCNAME[1]} call"
}

######################################################################
# Control functions
######

function add_marker {
    local marker=${1:-MARK}
    message "\n\n---------------\n ${marker} \n---------------\n\n"
}

function die {
    local code=${1:-1}
    local msg=""

    test -z "${ERR_CODES[${code}]}" && code=1
    printf "\n${ERR_MSGS[${code}]}\n"
    exit ${ERR_CODES[${code}]}
}

function backtrace {
    local -i frame=0

    while caller ${frame}
    do
        ((frame++))
    done

    printf "${*}\n"
    exit 1
}

function trap_errors {
    local errors=${@:-'1 2 3 4 5 6 9 11 15'}
    trap 'backtrace' ${errors}
}

function add_lock {
    local lockfile=${LOCKFILE:-}

    funcmark 
 
    test -z "${lockfile}" && {
        error "${lockfile} is not defined"
        exit 1
    }
    
    test_lock
    
    echo $$ > ${lockfile}
}

function remove_lock {
    local lockfile=${LOCKFILE:-}
    local cleanup='rm -f'
    
    funcmark
    
    test -z "${lockfile}" && {
        error "${lockfile} is not defined"
        exit 1
    } || test ! -f ${lockfile} && {
        error "${lockfile} does not exist"
        exit 1
    }

    test ${DEBUG} -ne 1 || cleanup="${cleanup} -v"

    ${cleanup} ${lockfile} || {
        error "Could not remove lock file: ${lockfile}"
        exit 1
    }
}

function test_lock {
    local lockfile=${LOCKFILE:-}
    local timeout=${TIMEOUT:-30}
    local exec_start=0
    local exec_stop=0
    local pid=0

    test -z "${lockfile}" && {
        error "${lockfile} is not defined"
        exit 1
    } || test -f ${lockfile} && {
        exec_start=$(date +%s)
        exec_stop=$((exec_start + timeout))
        pid=$(cat ${lockfile})

        test $(ps -o pid -p "${pid}" | wc -l) -gt 1 || {
            verbose "Removing stale lockfile"
            remove_lock
            return
        }

        while [[ $(date +%s) -lt ${exec_stop} ]]; do
            sleep 5
            test ! -f ${lockfile} && break
        done

        test -f ${lockfile} && {
            error "Lock-wait timeout: ${lockfile} is still present after ${timeout} seconds"
            exit 1
        }
    }
}

function getlongopt {
    local -a data=( $(echo "${@}") )
    local -i counter=0
    local -i setval=0
    local item=""
    local val=""

    set +e

    while [[ ${counter} -lt ${#data[@]} ]]
    do
        item=${data[${counter}]}
        val=""
        setval=0

        if [[ $(echo "${item}" | fgrep -c \=) -ne 0 ]]
        then
            setval=1
            val=$(echo "${item}" | cut -f2- -d=)
            item=$(echo "${item}" | cut -f1 -d=)
        fi

        case ${item} in
            debug|verbose)  enable_${item} ${val};;
        esac

        ((counter++))
    done

    set -e
}

function enable_debug {
    DEBUG=${1:-1}
    funcmark
    debug "Enabling debug mode"
}

function enable_verbose {
    VERBOSE=${1:-1}
    funcmark
    debug "Enabling verbose mode"
}

######################################################################
# Output manipulation
#####

function sum {
    awk '{SUM += $1} END {print SUM}'
}

function aggr {
    sort | uniq -c
}

function nospace {
    subschar '[[:space:]]'
}

function subspace {
    local char=${1:-,}
    subschar '[[:space:]]+' "${char}"    
}

function subschar {
    local pattern=${1:-na}
    local string=${2:-''}
    sed -r "s/${pattern}/${string}/g"
}

