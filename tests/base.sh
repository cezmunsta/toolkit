#!/bin/bash

source $(readlink $(dirname ${0})/../)lib/stdlib.sh

while getopts ':htdv-:' opt
do
    case ${opt} in
        -) getlongopt "${OPTARG[@]}";;
        d) DEBUG=1; enable_debug;; 
        v) VERBOSE=1; enable_verbose;;
        t) echo "Test arg OK";;
        *) error "Unknown option '${OPTARG}'"; exit 1;;
    esac
done

echo Done

